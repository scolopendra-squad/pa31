$(function () {

    if (window.location.pathname == '/') {
        var dot = $(".vertical__slider__inner .slick-active")
        setInterval(function () {
            if (!dot.hasClass('slick-active')) {
                dot = $(".vertical__slider__inner .slick-active");
                dotText = dot.children().text();
                $("div.intro__text-block").children().css("opacity", "0");
                setTimeout(function () {
                    $("div.intro__text-block").children().hide();
                    myElement = $('div.intro__text-block > div:nth-child(' + dotText + ')');

                    setTimeout(function () {
                        myElement.css("opacity", "1");
                    }, 200)

                    myElement.show();
                }, 200)
            }

        }, 100)

        var dot2 = $(".horizontal__slider__inner .slick-active")
        setInterval(function () {
            if (!dot2.hasClass('slick-active')) {
                dot2 = $(".horizontal__slider__inner .slick-active");
                dot2Text = dot2.children().text();
                $("div.news__text-block").children().css("opacity", "0");
                setTimeout(function () {
                    $("div.news__text-block").children().hide();
                    myElement = $('div.news__text-block > div:nth-child(' + dot2Text + ')');

                    setTimeout(function () {
                        myElement.css("opacity", "1");
                    }, 200)

                    myElement.show();
                }, 200)

            }

        }, 100)


        function slider() {
            $(".vertical__slider__inner .slick-prev").addClass("intro__btn intro__btn--pref");
            $(".vertical__slider__inner .slick-next").addClass("intro__btn intro__btn--next");

            $(".horizontal__slider__inner .slick-prev").addClass("news__btn news__btn--pref");
            $(".horizontal__slider__inner .slick-next").addClass("news__btn news__btn--next");


            $(".vertical__slider__inner ul.slick-dots").addClass("intro__dots");
            $(".horizontal__slider__inner .slick-dots").hide();
        }

        slider();

    }

    $('.solutions__section .button').on('click', function() {
        $('.solutions__section .button').removeClass("active");
        $(this).addClass("active");

        $('.solutions__item').removeClass("active");

        if ($(this).hasClass( "directions__button")) {
            $('.solutions__item.directions').addClass("active");
        }
        else {
            $('.solutions__item.industries').addClass("active");
        }
    });

    i = 1;
    $('.industries .col-2').children().each(function () {
        if (i != 1) {
            $(this).hide();
        }
        i++;
    });


    i = 1;
    $('.directions .col-2').children().each(function () {
        if (i != 1) {
            $(this).hide();
        }
        i++;
    });
});


//Функция отображения PopUp
function PopUpShow() {
    $("#popup1").show();
    setTimeout(function () {
        $("#popup1").addClass("active");
    }, 200)
}
//Функция скрытия PopUp
function PopUpHide() {
    $("#popup1").hide();
    $("#popup1").removeClass("active");
}

$('.footer__inner button').click(function() {
    PopUpShow();
});

$(document).click( function(e){
    if ($("#popup1").hasClass('active')) {
        if ($(e.target).closest('.b-popup-content').length) {
            // клик внутри элемента
            return;
        }
        PopUpHide();
    }
});


function showAll(num, tag) {
    $('.' + tag + ' .col-2').children().hide();
    $('.' + tag + ' .menu').children().removeClass('active');

    j = 1
    $('.' + tag + ' .menu').children().each(function () {
        if (j == num) {
            $(this).addClass('active');
        }
        j++;
    });

    i = 1;
    $('.' + tag + ' .col-2').children().each(function () {
        if (i == num) {
            $(this).show();
            $(this).addClass('active');
        }
        i++;
    });
}



function showText(n, pos) {
    $('.audit__buttons .texts').children().hide();
    $('.audit__buttons .hider').hide();
    $('.audit__buttons .texts1').children().hide();
    $('.audit__buttons .hider1').hide();
    $('.audit__buttons .items').children().removeClass('active');
    $('.audit__buttons .items1').children().removeClass('active');

    i = 1;
    $('.audit__buttons .texts').children().each(function () {
        if (i == n) {
            $(this).show();
        }
        i++;
    });

    i = 1;
    $('.audit__buttons .items').children().each(function () {
        if (i == n) {
            $(this).addClass('active');
        }
        i++;
    });

    i = 1;
    $('.audit__buttons .hider').each(function () {
        if (i == n) {
            $(this).show();
        }
        i++;
    });
}

function showText1(n) {
    $('.audit__buttons .texts').children().hide();
    $('.audit__buttons .texts1').children().hide();
    $('.audit__buttons .hider').hide();
    $('.audit__buttons .hider1').hide();
    $('.audit__buttons .items').children().removeClass('active');
    $('.audit__buttons .items1').children().removeClass('active');

    i = 1;
    $('.audit__buttons .texts1').children().each(function () {
        if (i == n) {
            $(this).show();
        }
        i++;
    });

    i = 1;
    $('.audit__buttons .items1').children().each(function () {
        if (i == n) {
            $(this).addClass('active');
        }
        i++;
    });

    i = 1;
    $('.audit__buttons .hider1').each(function () {
        if (i == n) {
            $(this).show();
        }
        i++;
    });
}


