@extends('layouts.app')

@section('title')
    @php ($title = "Направления")
@endsection


@section('content')
<!-- ABOUT -->
<div class="container">
	<div style="text-align: center; font-size: 32px; font-weight: bold; margin-top: 40px;">О <span style="color: #A50034;">системах менеджмента</span></div>

	<div style="display: flex; margin-top: 20px; margin-bottom: 120px">
		<div style="font-size: 20px; text-align: justify; width: calc(50% - 10px); margin-bottom: 80px">
			<p style="padding-bottom: 20px;">Система менеджмента ИСО (ISO Management System Standards, MSS) помогает организациям улучшить свои показатели, посредством разработки конкретных этапов, которым организации будут следовать в процессе собственной деятельности для достижения своих целей, а также создания организационной культуры.</p>
			<p>Под MSS понимаются добровольные стандарты, руководящие принципами или процессами, используемыми организациями для систематизации и легитимации разнообразных управленческих действий или задач.</p>
		</div>

		<div style="margin-left: 40px; padding: 0px 20px 0px 20px; border-radius: 20px; border-color: #EEEEEE; box-shadow: 4px 4px 10px rgb(50, 50, 50); width: calc(50% - 40px); position: absolute; right: 1%;">
			<img src="{{ asset('resources/img/iso_mss.jpg') }}" alt="Системы менеджмента ISO MSS" style="display: block; margin-left: auto; margin-right: auto; margin-top: 40px;  margin-bottom: 40px; width: calc(80%)">
		</div>
	</div>
</div>


<!-- ADVANTAGES -->
<div class="advantages">
    <div class="container">
        <div class="advantages__inner">
            <div class="advantages__title">
                ISO MSS: <span>Преимущества</span>
            </div>
            <div class="advantages__text">
                После внедрения системы менеджмента основными преимуществами для организаций будут:
            </div>
            <div class="advantages__cards">
                <div class="col">
                    <a href="#" class="item">
                        <div class="item__title">
                            Защита людей и окружающей среды
                        </div>
                        <div class="item__icon">
                            <img src="{{ asset('resources/img/helmet.png') }}">
                        </div>
                        <div class="item__text">
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem
                        </div>
                    </a>
                </div>
                <div class="col-long">
                    <a href="#" class="item">
                        <div class="item__title">
                            Повышение эффективности использования <br>ресурсов
                        </div>
                        <div class="item__icon">
                            <img src="{{ asset('resources/img/speedometer.png') }}">
                        </div>
                        <div class="item__text">
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero
                        </div>
                    </a>
                    <a href="#" class="item">
                        <div class="item__title">
                            Повышение их ценности для <br>заинтересованных сторон
                        </div>
                        <div class="item__icon">
                            <img src="{{ asset('resources/img/stats.png') }}">
                        </div>
                        <div class="item__text">
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero
                        </div>
                    </a>
                    <a href="#" class="item">
                        <div class="item__title">
                            Усовершенствование риск-менеджмента
                        </div>
                        <div class="item__icon">
                            <img src="{{ asset('resources/img/strategy.png') }}">
                        </div>
                        <div class="item__text">
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren.
                        </div>
                    </a>
                </div>
                <div class="col">
                    <a href="#" class="item">
                        <div class="item__title">
                            Улучшение финансовых показателей
                        </div>
                        <div class="item__icon">
                            <img src="{{ asset('resources/img/finance.png') }}">
                        </div>
                        <div class="item__text">
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<!---Кому поможет внедрение стандартов-->

<div class="who__standards__help">
    <div class="container">
        <div class="who__standards__help__inner">
            <div class="who__standards__help__title">
                Кому поможет <span>внедрение стандартов</span>
            </div>
            <div class="who__standards__help__inner__text">
                Стандарты системы менеджмента <span2>могут быть внедрены в любую организацию</span2>, большую или маленькую.<br><br>

                К числу наиболее распространенных стандартов систем менеджмента относятся: <br>
                <i class="far fa-dot-circle"></i> ISO 9001 – самый распространенный стандарт менеджмента качества в мире; <br>
                <i class="far fa-dot-circle"></i> ISO 14001 – самый распространенный стандарт экологического менеджмента в мире; <br>
                <i class="far fa-dot-circle"></i> ISO 45001 – первая в мире глобальная система менеджмента охраны здоровья и безопасности труда. <br><br>

                Максимального использования преимуществ систем менеджмента целесообразно <span3>обратиться за методологической поддержкой к экспертам</span3> <span>ООО «Профконсалт ИСМ»</span>, заказать корпоративный тренинг. <br>
                Наши консультанты имеют многолетний опыт внедрения, аудита и сопровождения систем менеджмента. Они предложат решение, наиболее подходящее для вашего предприятия при любом уровне сложности системы менеджмента и помогут избежать системных управленческих ошибок. <br>
                В фокусе внимания: качество продукции или услуг, эффективность работы, экологические показатели, охрана здоровья и безопасность труда.
            </div>
        </div>
    </div>
</div>






<!---Проведение аудитов-->
<div class="audit__in__management">
    <div class="container">
        <div class="audit__in__management__inner">
            <div class="audit__in__management__title">
                Проведение <span>аудитов</span>
            </div>
            <div class="audit__in__management__inner__text">
                Аудиты – важная часть системного подхода к управлению, поскольку они позволяют организации проверять, насколько их достижения соответствуют поставленным целям и демонстрируют соответствие стандарту. <br> <br>

                Консультанты ООО «Профконсалт ИСМ» готовы помочь в подготовке к аудиту, проведении внутреннего и внешнего аудита системы менеджмента, оказании необходимой методологической поддержки. <br> <br>

                В целях повышения эффективности внутреннего аудита консультанты ООО «Профконсалт ИСМ» проводят корпоративные и открытые тренинги, на темы внутреннего аудита по требованиям:
            </div>
            
            <div class="audit__buttons">
                <div class="col-1">
                    <div class="items">
                        <div class="item" onclick="showText(1)">ISO 9001
                        <div class="hider" style="display: none"></div>
                        </div>
                        <div class="item" onclick="showText(2)">ISO 14001
                            <div class="hider" style="display: none"></div>
                        </div>
                        <div class="item" onclick="showText(3)">ISO 45001
                            <div class="hider" style="display: none"></div>
                        </div>
                        <div class="item" onclick="showText(4)">ISO 22301
                            <div class="hider" style="display: none"></div>
                        </div>
                        <div class="item" onclick="showText(5)">ISO 37001
                            <div class="hider" style="display: none"></div>
                        </div>
                        <div class="item" onclick="showText(6)">ISO 27001
                            <div class="hider" style="display: none"></div>
                        </div>
                    </div>

                    <div class="texts">
                        <div class="item__text" style="display: none">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur cum doloribus eligendi enim eum explicabo fugit id, illo nihil nisi, obcaecati praesentium quam recusandae rerum suscipit tempore ut vel? Veniam? Lorem ipsum dolor sit amet, consectetur adipisicing elit. A exercitationem, minima. Accusantium aperiam atque culpa debitis doloremque, expedita incidunt inventore ipsum laborum nihil odio perspiciatis porro quis reiciendis rem saepe! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias architecto assumenda culpa deserunt distinctio dolor ea et excepturi laudantium maxime neque nisi, nostrum perspiciatis quos ratione recusandae repellat sed, tenetur.
                        </div>
                        <div class="item__text" style="display: none">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur cum doloribus fugit id, illo nihil nisi, obcaecati praesentium quam recusandae rerum suscipit tempore ut vel? Veniam? Lorem ipsum dolor sit amet, consectetur adipisicing elit. A exercitationem, minima. Accusantium aperiam atque culpa debitis doloremque, expedita incidunt inventore ipsum laborum nihil odio perspiciatis porro quis reiciendis rem saepe! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias architecto assumenda culpa deserunt ea et excepturi laudantium maxime neque nisi, nostrum perspiciatis quos ratione recusandae repellat sed, tenetur.
                        </div>
                        <div class="item__text" style="display: none">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur cum doloribus eligendi enim eum explicabo fugit id, illo nihil nisi, obcaecati praesentium quam recusandae rerum suscipit tempore ut vel? Veniam? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aperiam atque culpa debitis doloremque, expedita incidunt inventore ipsum laborum nihil odio perspiciatis porro quis reiciendis rem saepe! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias architecto assumenda culpa deserunt distinctio dolor ea et excepturi laudantium maxime neque nisi, nostrum perspiciatis quos ratione recusandae repellat sed, tenetur.
                        </div>
                        <div class="item__text" style="display: none">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur cum doloribus eligendi enim eum explicabo fugit id, illo nihil nisi, obcaecati praesentium quam recusandae rerum suscipit tempore ut vel? Veniam? Lorem ipsum dolor sit amet, consectetur adipisicing elit. A exercitationem, minima. Accusantium aperiam atque culpa debitis doloremque, expedita incidunt inventore ipsum laborum nihil odio perspiciatis porro quis reiciendis rem saepe! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias architecto assumenda culpa deserunt distinctio dolor ea et excepturi tenetur.
                        </div>
                        <div class="item__text" style="display: none">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur cum doloribus eligendi enim eum explicabo fugit id, illo nihil nisi, obcaecati praesentium quam recusandae rerum suscipit tempore ut vel? Veniam. A exercitationem, minima. Accusantium aperiam atque culpa debitis doloremque, expedita incidunt inventore ipsum laborum nihil odio perspiciatis porro quis reiciendis rem saepe! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias architecto assumenda culpa deserunt distinctio dolor ea et excepturi laudantium maxime neque nisi, nostrum perspiciatis quos ratione recusandae repellat sed, tenetur.
                        </div>
                        <div class="item__text" style="display: none">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur cum doloribus eligendi quam recusandae rerum suscipit tempore ut vel? Veniam? Lorem ipsum dolor sit amet, consectetur adipisicing elit. A exercitationem, minima. Accusantium aperiam atque culpa debitis doloremque, expedita incidunt inventore ipsum laborum nihil odio perspiciatis porro quis reiciendis rem saepe! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias architecto assumenda culpa deserunt distinctio dolor ea et excepturi laudantium maxime neque nisi, nostrum perspiciatis quos ratione recusandae repellat sed, tenetur.
                        </div>
                    </div>
                </div>

                <div class="col-2">
                    <div class="items1">
                        <div class="item" onclick="showText1(1)">ISO 9001
                            <div class="hider1" style="display: none"></div>
                        </div>
                        <div class="item" onclick="showText1(2)">ISO 14001
                            <div class="hider1" style="display: none"></div>
                        </div>
                        <div class="item" onclick="showText1(3)">ISO 45001
                            <div class="hider1" style="display: none"></div>
                        </div>
                        <div class="item" onclick="showText1(4)">ISO 22301
                            <div class="hider1" style="display: none"></div>
                        </div>
                        <div class="item" onclick="showText1(5)">ISO 37001
                            <div class="hider1" style="display: none"></div>
                        </div>
                        <div class="item" onclick="showText1(6)">ISO 27001
                            <div class="hider1" style="display: none"></div>
                        </div>
                    </div>

                    <div class="texts1">
                        <div class="item__text" style="display: none">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur CUM doloribus eligendi enim eum explicabo fugit id, illo nihil nisi, obcaecati praesentium quam recusandae rerum suscipit tempore ut vel? Veniam? Lorem ipsum dolor sit amet, consectetur adipisicing elit. A exercitationem, minima. Accusantium aperiam atque culpa debitis doloremque, expedita incidunt inventore ipsum laborum nihil odio perspiciatis porro quis reiciendis rem saepe! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias architecto assumenda culpa deserunt distinctio dolor ea et excepturi laudantium maxime neque nisi, nostrum perspiciatis quos ratione recusandae repellat sed, tenetur.
                        </div>
                        <div class="item__text" style="display: none">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur CUM doloribus fugit id, illo nihil nisi, obcaecati praesentium quam recusandae rerum suscipit tempore ut vel? Veniam? Lorem ipsum dolor sit amet, consectetur adipisicing elit. A exercitationem, minima. Accusantium aperiam atque culpa debitis doloremque, expedita incidunt inventore ipsum laborum nihil odio perspiciatis porro quis reiciendis rem saepe! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias architecto assumenda culpa deserunt ea et excepturi laudantium maxime neque nisi, nostrum perspiciatis quos ratione recusandae repellat sed, tenetur.
                        </div>
                        <div class="item__text" style="display: none">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur CUM doloribus eligendi enim eum explicabo fugit id, illo nihil nisi, obcaecati praesentium quam recusandae rerum suscipit tempore ut vel? Veniam? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aperiam atque culpa debitis doloremque, expedita incidunt inventore ipsum laborum nihil odio perspiciatis porro quis reiciendis rem saepe! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias architecto assumenda culpa deserunt distinctio dolor ea et excepturi laudantium maxime neque nisi, nostrum perspiciatis quos ratione recusandae repellat sed, tenetur.
                        </div>
                        <div class="item__text" style="display: none">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur CUM doloribus eligendi enim eum explicabo fugit id, illo nihil nisi, obcaecati praesentium quam recusandae rerum suscipit tempore ut vel? Veniam? Lorem ipsum dolor sit amet, consectetur adipisicing elit. A exercitationem, minima. Accusantium aperiam atque culpa debitis doloremque, expedita incidunt inventore ipsum laborum nihil odio perspiciatis porro quis reiciendis rem saepe! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias architecto assumenda culpa deserunt distinctio dolor ea et excepturi tenetur.
                        </div>
                        <div class="item__text" style="display: none">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur CUM doloribus eligendi enim eum explicabo fugit id, illo nihil nisi, obcaecati praesentium quam recusandae rerum suscipit tempore ut vel? Veniam. A exercitationem, minima. Accusantium aperiam atque culpa debitis doloremque, expedita incidunt inventore ipsum laborum nihil odio perspiciatis porro quis reiciendis rem saepe! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias architecto assumenda culpa deserunt distinctio dolor ea et excepturi laudantium maxime neque nisi, nostrum perspiciatis quos ratione recusandae repellat sed, tenetur.
                        </div>
                        <div class="item__text" style="display: none">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur CUM doloribus eligendi quam recusandae rerum suscipit tempore ut vel? Veniam? Lorem ipsum dolor sit amet, consectetur adipisicing elit. A exercitationem, minima. Accusantium aperiam atque culpa debitis doloremque, expedita incidunt inventore ipsum laborum nihil odio perspiciatis porro quis reiciendis rem saepe! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias architecto assumenda culpa deserunt distinctio dolor ea et excepturi laudantium maxime neque nisi, nostrum perspiciatis quos ratione recusandae repellat sed, tenetur.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--Интегрированная система менеджмента (ИСМ)-->

<div class="integrated__management__system">
    <div class="container">
        <div class="integrated__management__system__inner">
            <div class="integrated__management__system__title">
                Интегрированная система менеджмента <span>(ИСМ)</span>
            </div>
            <div class="integrated__management__system__inner__text">
                Услуги <span>ООО «Профконсалт ИСМ»</span> будут полезны организациям, предпочитающих использовать единую (иногда называемую «интегрированной») систему управления, которая отвечает требованиям двух или более стандартов системы управления одновременно. <br><br>
                Консультанты <span>ООО «Профконсалт ИСМ»</span> <span2>готовы оказать методологическое и практическое содействие</span2> при внедрении, подготовке к сертификации, сопровождении, и в случае необходимости – провести практико-ориентированный корпоративный тренинг или стратегическую сессию.
            </div>
            

        </div>
    </div>
</div>
@endsection
