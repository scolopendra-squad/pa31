@extends('layouts.app')

@section('title')
    @php ($title = "Решения")
@endsection


@section('content')
<!-- SOLUTIONS -->
<div class="solutions">
    <div class="container">
        <div class="solutions__inner">
            <div class="solutions__title">Мы готовы предоставить <span>решения</span></div>

            <div class="solutions__section">
                <div class="buttons">
                    <div class="button directions__button active">По <span>направлениям</span></div>
                    <div class="button industries__button">По <span>отраслям</span></div>
                </div>

                <div class="solutions__item directions active">
                    <div class="col-1">
                        <div class="menu">
                            <div class="menu__item" onclick="showAll(1, 'directions')">
                                <div class="icon"><i class="fas fa-shield-alt"></i></div>
                                <div class="menu__text active">Безопасность</div>
                            </div>
                            <div class="menu__item" onclick="showAll(2, 'directions')">
                                <div class="icon"><i class="fas fa-certificate"></i></div>
                                <div class="menu__text">Качество</div>
                            </div>
                            <div class="menu__item" onclick="showAll(3, 'directions')">
                                <div class="icon"><i class="fas fa-leaf"></i></div>
                                <div class="menu__text">Экология</div>
                            </div>
                            <div class="menu__item" onclick="showAll(4, 'directions')">
                                <div class="icon"><i class="fas fa-power-off"></i></div>
                                <div class="menu__text">Энергоэффективность</div>
                            </div>
                            <div class="menu__item" onclick="showAll(5, 'directions')">
                                <div class="icon"><i class="fas fa-hamburger"></i></div>
                                <div class="menu__text">Пищевая
                                    безопасность</div>
                            </div>
                            <div class="menu__item" onclick="showAll(6, 'directions')">
                                <div class="icon"><i class="fas fa-coins"></i></div>
                                <div class="menu__text">Управление
                                    активами</div>
                            </div>
                            <div class="menu__item" onclick="showAll(7, 'directions')">
                                <div class="icon"><i class="fas fa-file"></i></div>
                                <div class="menu__text">Комплаенс
                                    риски</div>
                            </div>
                            <div class="menu__item" onclick="showAll(8, 'directions')">
                                <div class="icon"><i class="fas fa-shield-alt"></i></div>
                                <div class="menu__text">Информационная
                                    безопасноть</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-2">
                        <div class="one__block">
                            <div class="text-block">
                                Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                            </div>

                            <div class="doc-block">
                                <div class="documents">
                                    <div class="title">Документы по этой теме:</div>
                                    <div class="documents__list">
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ПАБ</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">АКП</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ISO 39000</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ISO 45001</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="short__text">

                                </div>
                            </div>
                        </div>

                        <div class="one__block">
                            <div class="text-block">
                                At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                            </div>

                            <div class="doc-block">
                                <div class="documents">
                                    <div class="title">Документы по этой теме:</div>
                                    <div class="documents__list">
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ПАБ</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">АКП</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ISO 39000</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ISO 45001</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="short__text">

                                </div>
                            </div>
                        </div>

                        <div class="one__block">
                            <div class="text-block">
                                At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                            </div>

                            <div class="doc-block">
                                <div class="documents">
                                    <div class="title">Документы по этой теме:</div>
                                    <div class="documents__list">
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ПАБ</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">АКП</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ISO 39000</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ISO 45001</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="short__text">

                                </div>
                            </div>
                        </div>

                        <div class="one__block">
                            <div class="text-block">
                                At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                            </div>

                            <div class="doc-block">
                                <div class="documents">
                                    <div class="title">Документы по этой теме:</div>
                                    <div class="documents__list">
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ПАБ</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">АКП</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ISO 39000</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ISO 45001</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="short__text">

                                </div>
                            </div>
                        </div>

                        <div class="one__block">
                            <div class="text-block">
                                At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.
                            </div>

                            <div class="doc-block">
                                <div class="documents">
                                    <div class="title">Документы по этой теме:</div>
                                    <div class="documents__list">
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ПАБ</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">АКП</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ISO 39000</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ISO 45001</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="short__text">

                                </div>
                            </div>
                        </div>

                        <div class="one__block">
                            <div class="text-block">
                                At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
                            </div>

                            <div class="doc-block">
                                <div class="documents">
                                    <div class="title">Документы по этой теме:</div>
                                    <div class="documents__list">
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ПАБ</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">АКП</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ISO 39000</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ISO 45001</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="short__text">

                                </div>
                            </div>
                        </div>

                        <div class="one__block">
                            <div class="text-block">
                                At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                            </div>

                            <div class="doc-block">
                                <div class="documents">
                                    <div class="title">Документы по этой теме:</div>
                                    <div class="documents__list">
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ПАБ</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">АКП</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ISO 39000</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ISO 45001</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="short__text">

                                </div>
                            </div>
                        </div>

                        <div class="one__block">
                            <div class="text-block">
                                At vero eos et accusam et justo duo dolores et ea rebum. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                            </div>

                            <div class="doc-block">
                                <div class="documents">
                                    <div class="title">Документы по этой теме:</div>
                                    <div class="documents__list">
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ПАБ</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">АКП</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ISO 39000</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ISO 45001</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="short__text">

                                </div>
                            </div>
                        </div>

                    </div>
                </div>



                <div class="solutions__item industries">
                    <div class="col-1">
                        <div class="menu">
                            <div class="menu__item  active" onclick="showAll(1, 'industries')">
                                <div class="icon"><i class="fas fa-shield-alt"></i></div>
                                <div class="menu__text">Пищевая</div>
                            </div>
                            <div class="menu__item" onclick="showAll(2, 'industries')">
                                <div class="icon"><i class="fas fa-certificate"></i></div>
                                <div class="menu__text">Металлургическая</div>
                            </div>
                            <div class="menu__item" onclick="showAll(3, 'industries')">
                                <div class="icon"><i class="fas fa-leaf"></i></div>
                                <div class="menu__text">Нефте-газовая</div>
                            </div>
                            <div class="menu__item" onclick="showAll(4, 'industries')">
                                <div class="icon"><i class="fas fa-power-off"></i></div>
                                <div class="menu__text">Авто- и авиа</div>
                            </div>
                            <div class="menu__item" onclick="showAll(5, 'industries')">
                                <div class="icon"><i class="fas fa-hamburger"></i></div>
                                <div class="menu__text">Горнодобывающая</div>
                            </div>
                            <div class="menu__item" onclick="showAll(6, 'industries')">
                                <div class="icon"><i class="fas fa-coins"></i></div>
                                <div class="menu__text">Производственная</div>
                            </div>
                            <div class="menu__item" onclick="showAll(7, 'industries')">
                                <div class="icon"><i class="fas fa-file"></i></div>
                                <div class="menu__text">Энергетическая</div>
                            </div>
                            <div class="menu__item" onclick="showAll(8, 'industries')">
                                <div class="icon"><i class="fas fa-shield-alt"></i></div>
                                <div class="menu__text">Оказания услуг</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-2">
                        <div class="one__block">
                            <div class="text-block">
                                Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                            </div>

                            <div class="doc-block">
                                <div class="documents">
                                    <div class="title">Документы по этой теме:</div>
                                    <div class="documents__list">
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ПАБ</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">АКП</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ISO 39000</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ISO 45001</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="short__text">

                                </div>
                            </div>
                        </div>

                        <div class="one__block">
                            <div class="text-block">
                                At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                            </div>

                            <div class="doc-block">
                                <div class="documents">
                                    <div class="title">Документы по этой теме:</div>
                                    <div class="documents__list">
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ПАБ</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">АКП</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ISO 39000</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ISO 45001</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="short__text">

                                </div>
                            </div>
                        </div>

                        <div class="one__block">
                            <div class="text-block">
                                At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                            </div>

                            <div class="doc-block">
                                <div class="documents">
                                    <div class="title">Документы по этой теме:</div>
                                    <div class="documents__list">
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ПАБ</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">АКП</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ISO 39000</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ISO 45001</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="short__text">

                                </div>
                            </div>
                        </div>

                        <div class="one__block">
                            <div class="text-block">
                                At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                            </div>

                            <div class="doc-block">
                                <div class="documents">
                                    <div class="title">Документы по этой теме:</div>
                                    <div class="documents__list">
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ПАБ</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">АКП</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ISO 39000</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ISO 45001</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="short__text">

                                </div>
                            </div>
                        </div>

                        <div class="one__block">
                            <div class="text-block">
                                At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.
                            </div>

                            <div class="doc-block">
                                <div class="documents">
                                    <div class="title">Документы по этой теме:</div>
                                    <div class="documents__list">
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ПАБ</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">АКП</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ISO 39000</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ISO 45001</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="short__text">

                                </div>
                            </div>
                        </div>

                        <div class="one__block">
                            <div class="text-block">
                                At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
                            </div>

                            <div class="doc-block">
                                <div class="documents">
                                    <div class="title">Документы по этой теме:</div>
                                    <div class="documents__list">
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ПАБ</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">АКП</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ISO 39000</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ISO 45001</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="short__text">

                                </div>
                            </div>
                        </div>

                        <div class="one__block">
                            <div class="text-block">
                                At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                            </div>

                            <div class="doc-block">
                                <div class="documents">
                                    <div class="title">Документы по этой теме:</div>
                                    <div class="documents__list">
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ПАБ</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">АКП</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ISO 39000</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ISO 45001</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="short__text">

                                </div>
                            </div>
                        </div>

                        <div class="one__block">
                            <div class="text-block">
                                At vero eos et accusam et justo duo dolores et ea rebum. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                            </div>

                            <div class="doc-block">
                                <div class="documents">
                                    <div class="title">Документы по этой теме:</div>
                                    <div class="documents__list">
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ПАБ</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">АКП</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ISO 39000</div>
                                        </div>
                                        <div class="item">
                                            <div class="icon"><i class="fas fa-book-open"></i></div>
                                            <div class="text">ISO 45001</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="short__text">

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
