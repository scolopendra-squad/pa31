@extends('layouts.app')

@section('title')
    @php ($title = "О нас")
@endsection


@section('content')
<!-- INTRO -->
<div class="intro">
    <div class="intro__inner">
        <div class="intro__container container">
            <div class="intro__text-block">
                <div class="intro__text-block-item">
                    <div class="intro__text">
                        <div class="intro__title">Флагманский продукт 1</div>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At</p>
                    </div>
                    <div class="intro__button style__button"><button>Подобрать решение</button></div>
                </div>

                <div class="intro__text-block-item" style="display: none">
                    <div class="intro__text">
                        <div class="intro__title">Флагманский продукт 2</div>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At</p>
                    </div>
                    <div class="intro__button style__button"><button>Подобрать решение</button></div>
                </div>

                <div class="intro__text-block-item" style="display: none">
                    <div class="intro__text">
                        <div class="intro__title">Флагманский продукт 3</div>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At</p>
                    </div>
                    <div class="intro__button style__button"><button>Подобрать решение</button></div>
                </div>
            </div>
        </div>

        <div class="intro__slider">
            <div class="vertical__slider__inner">

                <div class="slider__item"><div class="slider__img"><img src="{{ asset('resources/img/intro_slider.jpg') }}" alt=""></div></div>
                <div class="slider__item"><div class="slider__img"><img src="{{ asset('resources/img/intro_slider.jpg') }}" alt=""></div></div>
                <div class="slider__item"><div class="slider__img"><img src="{{ asset('resources/img/intro_slider.jpg') }}" alt=""></div></div>

            </div>
        </div>
    </div>
</div>




<!-- COMPANY IN NUMBERS -->
<div class="company__in__numbers">
    <div class="container">
        <div class="company__in__numbers_inner">
            <div class="company__in__numbers__title">
                О компании <span>в цифрах</span>
            </div>
            <div class="company__in__numbers__cards">
                <div class="company__in__numbers__item">
                    <div class="cards__item-inner">
                        <div class="company__in__numbers__cards__text">Уже<br> <span>10</span><br> лет на рынке</div>
                    </div>
                </div>
                <div class="company__in__numbers__item">
                    <div class="cards__item-inner">
                        <div class="company__in__numbers__cards__text">Более<br> <span>50</span><br> партнеров</div>
                    </div>
                </div>
                <div class="company__in__numbers__item">
                    <div class="cards__item-inner">
                        <div class="company__in__numbers__cards__text">Свыше<br> <span>1000</span><br> проектов</div>
                    </div>
                </div>
                <div class="company__in__numbers__item">
                    <div class="cards__item-inner">
                        <div class="company__in__numbers__cards__text">Эффект <br>от <span>2</span> <span2>млн. руб.</span2><br> для одного<br>проекта<br>c Lean 6 Sigma</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- WHY US -->
<div class="why-us">
    <div class="container">
        <div class="why-us__inner">
            <div class="why-us__title">Почему именно Профконсалт <span>ИСМ</span>?</div>
            <div class="cards__inner">
                <div class="cards__item">
                    <div class="cards__item-inner">
                        <div class="cards__icon"><img src="{{ asset('resources/img/calendar.png') }}" alt=""></div>
                        <div class="cards__text">Команда консультантов-профессионалов с многолетним опытом</div>
                    </div>
                </div>

                <div class="cards__item">
                    <div class="cards__item-inner">
                        <div class="cards__icon"><img src="{{ asset('resources/img/calendar.png') }}" alt=""></div>
                        <div class="cards__text">Команда консультантов-профессионалов с многолетним опытом</div>
                    </div>
                </div>

                <div class="cards__item">
                    <div class="cards__item-inner">
                        <div class="cards__icon"><img src="{{ asset('resources/img/calendar.png') }}" alt=""></div>
                        <div class="cards__text">Команда консультантов-профессионалов с многолетним опытом</div>
                    </div>
                </div>

                <div class="cards__item">
                    <div class="cards__item-inner">
                        <div class="cards__icon"><img src="{{ asset('resources/img/calendar.png') }}" alt=""></div>
                        <div class="cards__text">Команда консультантов-профессионалов с многолетним опытом</div>
                    </div>
                </div>

                <div class="cards__item">
                    <div class="cards__item-inner">
                        <div class="cards__icon"><img src="{{ asset('resources/img/calendar.png') }}" alt=""></div>
                        <div class="cards__text">Команда консультантов-профессионалов с многолетним опытом</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--
<div class="why-us">
    <div class="container">
        <div class="why-us__inner">
            <div class="why-us__title">Почему именно Профконсалт <span>ИСМ</span>?</div>
            <div class="cards__inner">
                <div class="cards__item">
                    <div class="cards__item-inner">
                        <div class="cards__title">Уже <br><span>10 лет</span><br> на рынке</div>
                        <div class="cards__icon"><img src="img/calendar.png" alt=""></div>
                        <div class="cards__text">ООО "Профконсалт ИСМ" оказывает консалтинговые услуги с 2010 года</div>
                    </div>
                </div>

                <div class="cards__item">
                    <div class="cards__item-inner">
                        <div class="cards__title">Более <br><span>50</span><br> партнеров</div>
                        <div class="cards__icon"><img src="img/networking.png" alt=""></div>
                        <div class="cards__text">уже увеличили свой доход, благодаря использованию Lean 6 Sigma</div>
                    </div>
                </div>

                <div class="cards__item">
                    <div class="cards__item-inner">
                        <div class="cards__title">Свыше <br><span>1000</span><br> проектов</div>
                        <div class="cards__icon"><img src="img/workflow.png" alt=""></div>
                        <div class="cards__text">стали более эффективными и приносят увеличенную прибыль владельцам</div>
                    </div>
                </div>

                <div class="cards__item">
                    <div class="cards__item-inner">
                        <div class="cards__title">Эффект <br><span>от 2 до 230</span><br> млн руб.</div>
                        <div class="cards__icon"><img src="img/wallet.png" alt=""></div>
                        <div class="cards__text">достигается для одного проекта с Lean 6 Sigma</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
-->



<!-- NEWS -->
<div class="news">
    <div class="news__title-block">Что <span>нового</span>?</div>
    <div class="news__inner">
        <div class="news__container container">
            <div class="news__text-block">
                <div class="news__text-block-item">
                    <div class="news__text">
                        <div class="news__title">Проведены подсчеты за 2020 год</div>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit</p>
                    </div>
                    <div class="news__button style__button"><button>Читать подробнее &#10140;</button></div>
                </div>

                <div class="news__text-block-item" style="display: none">
                    <div class="news__text">
                        <div class="news__title">Проведены подсчеты за 2019 год</div>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit</p>
                    </div>
                    <div class="news__button style__button"><button>Читать подробнее &#10140;</button></div>
                </div>

                <div class="news__text-block-item" style="display: none">
                    <div class="news__text">
                        <div class="news__title">Проведены подсчеты за 2018 год</div>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit</p>
                    </div>
                    <div class="news__button style__button"><button>Читать подробнее &#10140;</button></div>
                </div>
            </div>
        </div>

        <div class="news__slider">
            <div class="horizontal__slider__inner">

                <div class="slider__item"><div class="slider__img"><img src="{{ asset('resources/img/news_slider.jpg') }}" alt=""></div></div>
                <div class="slider__item"><div class="slider__img"><img src="{{ asset('resources/img/news_slider.jpg') }}" alt=""></div></div>
                <div class="slider__item"><div class="slider__img"><img src="{{ asset('resources/img/news_slider.jpg') }}" alt=""></div></div>

            </div>
        </div>
    </div>
</div>


<!-- POLITICIANS -->
<div class="politicians">
    <div class="container">
        <div class="politicians__inner">
            <div class="politicians__title">
                Политики Профконсалт <span>ИСМ</span>
            </div>
            <div class="politicians__cards">
                <div class="col">
                    <a href="#" class="item">
                        <div class="item__title">Качество</div>
                        <div class="item__icon"><img src="{{ asset('resources/img/best-choice.png') }}"></div>
                        <div class="item__text">
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat
                        </div>
                    </a>
                </div>
                <div class="col-long">
                    <a href="#" class="item">
                        <div class="item__title">Конфиденциальность</div>
                        <div class="item__icon"><img src="{{ asset('resources/img/privacy-policy.png') }}"></div>
                        <div class="item__text">
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet
                        </div>
                    </a>
                    <a href="#" class="item">
                        <div class="item__title">Безопасность</div>
                        <div class="item__icon"><img src="{{ asset('resources/img/cyber-security.png') }}"></div>
                        <div class="item__text">
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet
                        </div>
                    </a>
                </div>
                <div class="col">
                    <a href="#" class="item">
                        <div class="item__title">Экология</div>
                        <div class="item__icon"><img src="{{ asset('resources/img/biodegradable.png') }}"></div>
                        <div class="item__text">
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- MAP -->
<div class="map">
    <div class="container">
        <div class="map__title">
                География наших <span>проектов</span>
            </div>
        <div class="map__inner">
            <div class="block"></div>
            <div class="description" id="RU" style="display: none">
                RU. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit.
            </div>
            <div class="description" id="BR" style="display: none">
                BR. At vero eos et accusam et justo duo dolores et ea rebum. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit.
            </div>
            <div class="description" id="IN" style="display: none">
                IN. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
            </div>
            <div class="description" id="CN" style="display: none">
                CN. At vero eos et. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit. At vero eos et accusam et justo duo dolores et ea rebum.
            </div>
            <div class="description" id="US" style="display: none">
                US. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit.
            </div>
            <div class="description" id="UK" style="display: none">
                UK. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit.
            </div>
            <div id="map"></div>
        </div>
    </div>
</div>


<!-- OUR PARTNERS -->
<div class="our__partners">
    <div class="container">
        <div class="our__partners__inner">
            <div class="our__partners__title">
                Наши <span>Партнеры</span>
            </div>
            <div class="our__partners__cards">
                <div class="item">
                    <div class="item__icon">
                        <img src="{{ asset('resources/img/img_partners.png') }}">
                    </div>
                </div>
                <div class="item">
                    <div class="item__icon">
                        <img src="{{ asset('resources/img/img_partners.png') }}">
                    </div>
                </div>
                <div class="item">
                    <div class="item__icon">
                        <img src="{{ asset('resources/img/img_partners.png') }}">
                    </div>
                </div>
                <div class="item">
                    <div class="item__icon">
                        <img src="{{ asset('resources/img/img_partners.png') }}">
                    </div>
                </div>
                <div class="item">
                    <div class="item__icon">
                        <img src="{{ asset('resources/img/img_partners.png') }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




@endsection
