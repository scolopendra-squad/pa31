@extends('layouts.app')

@section('title')
    @php ($title = "ЛИН 6 Направления")
@endsection


@section('content')
<!-- ABOUT -->
<div class="container">

	<div style="text-align: center; font-size: 32px; font-weight: bold; margin-top: 40px;">Описание <span style="color: #A50034;">продукта</span></div>

	<div style="display: flex; margin-top: 40px; margin-bottom: 80px">
		<div style="font-size: 20px; width: calc(50% - 10px); margin-bottom: 120px; margin-top: 60px">
			<div style="text-align: center; font-size: 44px; font-weight: bold; margin-top: 40px; ">Решения, которые <span style="color: #A50034;">работают</span></div>
			<p style="text-align: justify;">Увеличение скорости процессов и устранение потерь, создание/реанимация производственной системы, развертывание стратегии Lean&Six Sigma. Решение основано на методологии Lean и методологии Six Sigma</p>
		</div>

		<div style="margin-left: 40px; padding: 0px 20px 0px 20px; border-radius: 20px; border-color: #EEEEEE; box-shadow: 4px 4px 10px rgb(50, 50, 50); width: calc(50% - 40px); position: absolute; right: 1%;">
			<img src="{{ asset('resources/img/lean_six_sigma.jpg') }}" alt="Системы менеджмента ISO MSS" style="display: block; margin-left: auto; margin-right: auto; margin-top: 40px;  margin-bottom: 40px; width: calc(80%)">
		</div>
	</div>
</div>


<!-- PRODUCT INFORMATION -->
<div style="background: rgba(211, 188, 141, 0.1);">
	<div class="container">
		<div style="text-align: center; font-size: 32px; font-weight: bold; padding-top: 20px; padding-bottom: 20px">Особенности <span style="color: #A50034;">продукта</span></div>
		
		<div style="display: inline-flex; flex-direction: row; justify-content: center; padding-bottom: 20px">
			<div style="display: flex; width: calc(50% - 10px); margin: 10px 40px 10px 10px; padding: 10px; border-radius: 0 10px; box-shadow: 4px 4px 10px rgb(165, 0, 0); border: #A50034 1px solid;">
				<div>
					<img src="{{ asset('resources/img/finance.png') }}" width = 100 style="margin: 10px; transform: translateY(50%);">
				</div>
				
				<div>
					<h2 style="font-size: 28px; text-align: center;">В фокусе внимания -<br>экономический эффект</h2>
					<p style="font-size: 20px; text-align: justify; padding: 10px">В отличие от подходов ФЦК к внедрению методологии Бережливого производства, которые сфокусированы на увеличении производительности, <span style="color: #A50034;">ООО «Профконсалт ИСМ»</span> сосредоточены на экономическом эффекте и повышение производительности это одни из способов его получения.</p>
				</div>
			</div>
			
			<div style="display: flex; width: calc(50% - 10px); margin: 10px; padding: 10px; border-radius: 10px 0; box-shadow: 4px 4px 10px rgb(165, 0, 0); border: #A50034 1px solid;">
				<div>
					<img src="{{ asset('resources/img/networking.png') }}" width = 100 style="margin: 10px; transform: translateY(50%);">
				</div>
				
				<div>
					<h2 style="font-size: 28px; text-align: center;">Использование «умных<br>данных»‎ для принятия<br>управленческих решений</h2>
					<p style="font-size: 20px; text-align: justify; padding: 10px">Эксперты <span style="color: #A50034;">ООО «Профконсалт ИСМ»</span> обладают экспертизой в применении программного обеспечения Minitab и мы делаем акцент на статистическом управлении производством (этого нет в подходах ФЦК и нет в методологии Бережливого производства)</p>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- LEAN AND SIX SIGMA METHODOLOGIES -->
<div class="container">
	<div style="display: flex; flex-direction: column;">
		<div style="text-align: center; font-size: 32px; font-weight: bold; margin-top: 40px;">Методологии <span style="color: #A50034;">Lean и Six Sigma</span></div>
		
		<div style="display: inline-flex; flex-direction: row; justify-content: center; margin-top: 40px; margin-bottom: 40px;">
			<div style="display: flex; flex-direction: column; width: calc(30%); padding: 10px; border-radius: 10px; box-shadow: 4px 4px 10px rgb(165, 0, 0); border: #A50034 1px solid;">
				<div>
					<img src="{{ asset('resources/img/lean_logo.png') }}" style="display: block; margin-left: auto; margin-right: auto; margin-top: 40px;  margin-bottom: 40px;">
				</div>
				<div style="font-size: 20px; text-align: center; padding: 10px"><p><b>Методология Lean</b><br> сосредоточена на увеличении скорости процессов:</p>
					<p>- Упрощение процессов</p>
					<p>- Фокусирование внимания на сквозных процессах</p>
					<p>- Устранение действий не связанных с добавленной стоимостью</p>
				</div>
			</div>
			
			<div style="width: calc(5%); text-align: center; margin-top: auto; margin-bottom: auto; color: #A50034">
				<span>
					<i class="fas fa-chevron-right fa-2x"></i>
					<i class="fas fa-chevron-right fa-2x"></i>
				</span>
			</div>
			
			<div style="display: flex; flex-direction: column; width: calc(30%); padding: 10px; border-radius: 10px; box-shadow: 4px 4px 10px rgb(165, 0, 0); border: #A50034 1px solid;">
				<div>
					<img src="{{ asset('resources/img/lean_six_sigma_logo.png') }}" style="display: block; margin-left: auto; margin-right: auto; margin-top: 40px;  margin-bottom: 40px;">
				</div>
				<div style="font-size: 20px; text-align: center; padding: 10px"><p><b>Методология  Lean Six Sigma</b><br> Их часто применяют вместе, что позволяет предоставить клиенту совершенный продукт.</p>
					<p>- Комбинирование методологий создает лучший подход для повышения эффективности бизнеса</p>
					<p>- Подход, используемый для улучшения процессов и грамотного проектирования для будущего роста</p>
				</div>
			</div>
			
			<div style="width: calc(5%); text-align: center; margin-top: auto; margin-bottom: auto; color: #A50034">
				<span>
					<i class="fas fa-chevron-left fa-2x"></i>
					<i class="fas fa-chevron-left fa-2x"></i>
				</span>
			</div>
			
			<div style="display: flex; flex-direction: column; width: calc(30%);padding: 10px; border-radius: 10px; box-shadow: 4px 4px 10px rgb(165, 0, 0); border: #A50034 1px solid;">
				<div>
					<img src="{{ asset('resources/img/six_sigma_logo.png') }}" style="display: block; margin-left: auto; margin-right: auto; margin-top: 40px;  margin-bottom: 40px;">
				</div>
				<div style="font-size: 20px; text-align: center; padding: 10px"><p><b>Методология Six Sigma</b><br> сосредоточена на повышении качества продукции:</p>
						<p>- Сокращение вариаций</p>
						<p>- Постоянное совершенствование</p>
						<p>- Решение проблем и управление бизнесом с использованием подхода, основанного на данных</p>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- OUR TRAINING PROGRAMS -->
<div style="background: rgba(211, 188, 141, 0.1);">
	<div class="container">
		<div style="text-align: center; font-size: 32px; font-weight: bold; padding-top: 20px; padding-bottom: 20px">Наши <span style="color: #A50034;">тренинги</span></div>
		
		<h1 style="text-align: center; color: #A50034;"> В РАЗРАБОТКЕ</h1>
		
	</div>
</div>


<!-- PRACTICAL TRAINING PROGRAMS / COACHING -->
<div class="container">
	<div style="display: flex; flex-direction: column;">
		<div style="text-align: center; font-size: 32px; font-weight: bold; margin-top: 40px;">Практические <span style="color: #A50034;">тренинги/коучинг</span></div>
		
		<div style="padding: 20px; font-size: 20px">
			<p style="margin-bottom: 20px">
				Режим реализации Проекта и проведения тренинга Green Belts предполагает, что эксперт <span style="color: #A50034;">ООО «Профконсалт ИСМ»</span>
				в большей части фокусируется на передаче знаний («ноу-хау»), то есть на том, как реализуются проекты Lean Six Sigma: 
			</p>
			<p><i class="fas fa-chevron-right"></i> эксперт <span style="color: #A50034;">ООО «Профконсалт ИСМ»</span> обучает, развивает и сопровождает косанду (рабочую группу) проекта</p>
			<p><i class="fas fa-chevron-right"></i> члены команды, являющиеся сотрудниками Заказчика, выполняют большинство задач Проекта самостоятельно
					 (эксперт <span style="color: #A50034;">ООО «Профконсалт ИСМ»</span> осуществляет коучинг и сопровождение, а члены команды оптимизируют процесс 
					 и одновременно учатся практической реализации проектов Lean Six Sigma)</p>
			<p><i class="fas fa-chevron-right"></i> по окончании проекта ответственные сотрудники Заказчика имеют собственные компетенции по оптимизации других процессов</p>
		</div>
	</div>
</div>



<!-- PROJECT IMPLEMENTATION -->
<div style="background: rgba(211, 188, 141, 0.1);">
	<div class="container">
		<div style="display: flex; flex-direction: column;">
			<div style="text-align: center; font-size: 32px; font-weight: bold; padding-top: 20px; padding-bottom: 20px">Реализация <span style="color: #A50034;">проектов</span></div>
			
			<div style="padding: 20px; font-size: 20px">
				<p>Применение Lean Six Sigma  в бизнесе. Крупнейшие мировые корпорации в различных секторах экономики повышают качество и 
				рентабельность бизнеса, используя технологию Lean Six Sigma.</p>
				<p style="line-height: 60px; font-weight: bold">Проекты могут быть в <span style="color: #A50034;"><i><u>различных отраслях</u></i></span>.</p>
			</div>
		</div>
	</div>
</div>
@endsection