@yield('title')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="{{ asset('resources/css/style.css') }}" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="{{ asset('resources/css/svgworldmap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <script src="https://kit.fontawesome.com/d22ccd8569.js" crossorigin="anonymous"></script>
    <title>{{ $title }}</title>
</head>
<body>

<!-- HEADER -->
<header style="position: sticky;">
    <div class="container">
        <div class="header__inner">
            <div class="header__up">
                <div class="logo">
                   
                    <div class="logo__img"><object type="image/svg+xml" height="100" width="94.88" data="{{ asset('resources/img/logo.svg') }}"></object></div>
                    <div class="logo__text">Профконсалт <span>ИСМ</span></div>
                </div>
                <div class="tagline">Решения, которые <span>работают</span>!</div>
                <div class="style__button"><button>Подобрать решение</button></div>
            </div>
        </div>

        <nav>
            <a class="nav__link" href="{{ route('aboutUs' )}}">О нас</a>
            <div class="nav__link dropdown">
                <a class="nav__link" href="#">Системы менеджмента</a>
                <div class="dropdown__section">
                    <div class="item">
                        <div class="icon"><i class="far fa-dot-circle"></i></div>
                        <a class="text" href="{{ route('directions' )}}">Направления</a>
                    </div>

                    <div class="item">
                        <div class="icon"><i class="far fa-dot-circle"></i></div>
                        <a class="text" href="{{ route('competence' )}}">Компетенции</a>
                    </div>
                </div>
            </div>
            <div class="nav__link dropdown">
                <a class="nav__link" href="#">ЛИН 6 Сигма</a>
                <div class="dropdown__section">
                    <div class="item">
                        <div class="icon"><i class="far fa-dot-circle"></i></div>
                        <a class="text" href="{{ route('lean_six_directions' )}}">Направления</a>
                    </div>

                    <div class="item">
                        <div class="icon"><i class="far fa-dot-circle"></i></div>
                        <a class="text" href="{{ route('competence' )}}">Компетенции</a>
                    </div>
                </div>
            </div>
            <a class="nav__link" href="#">Онлайн школа</a>
            <a class="nav__link" href="#">Консультация экперта</a>
			<a class="nav__link" href="#">Публикации</a>
            <a class="nav__link" href="#">Контакты</a>
        </nav>
    </div>
</header>

@yield('content')


<!-- FOOTER -->
<footer>
    <div class="footer__gray">
        <div class="container">
            <div class="footer__inner">
                <div class="text__in__gray__footer">Остались вопросы? Нужны подробности?</div>
                <div class="style__button"><button>Свяжитесь с нами</button></div>
            </div>
        </div>
    </div>
    <div class="footer__black">
        <div class="container">
            <div class="footer__inner">
                <div class="col__1">
                    <div class="logo__img"><object type="image/svg+xml" height="90" width="94.88" data="{{ asset('resources/img/logo.svg') }}"></object></div>
                    <div class="footer__text__main">Профконсалт <span>ИСМ</span>
                    </div>
                    <div class="copyright">&copy; Profconsult MMI 2020</div>
                </div>
                <div class="col__2">
                    <div class="footer__text">Профконсалт ИСМ в соцсетях: </div>
                    <div class="footer__icons">
                        <a href="#"><i class="fab fa-vk"></i></a>
                        <a href="#"><i class="fab fa-telegram-plane"></i></a>
                        <a href="#"><i class="fab fa-twitter"></i></a>
                        <a href="#"><i class="fab fa-facebook-f"></i></a>
                        <a href="#"><i class="fab fa-linkedin"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="icon__links">Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
    </div>
</footer>


<!-- POPUP -->
<div class="b-popup" id="popup1" style="display: none">
    <div class="b-popup-content">
        <div class="popup__title">
            Связаться с нами
        </div>
        <div class="popup__forms">
            <input class="input__name" placeholder="Ваше имя">
            <input class="input__number" placeholder="Ваш номер телефона">
            <input class="input__email" placeholder="Ваш адрес электронной почты">
            <textarea rows="3" class="comment" placeholder="Ваш комментарий к заявке"></textarea>
        </div>
        <div class="checkboxes">
            <div class="personal__data">
                <input type="checkbox" id="personal__data">
                <label for="personal__data">Я согласен на обработку персональных данных</label>
            </div>

            <div class="privacy__policy">
                <input type="checkbox" id="privacy__policy">
                <label for="privacy__policy">Я согласен с условиями <span>политики конфиденциальности</span></label>
            </div>
        </div>

        <div class="style__button"><button>Заказать звонок</button></div>
    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script type="text/javascript" src="{{ asset('resources/js/jquery.svgworldmap-2.1.src.js') }}"></script>
<script src="{{ asset('resources/js/script.js') }}"></script>

<script type="text/javascript">
    $('.vertical__slider__inner').slick({
        vertical: true,
        dots: true,
        verticalSwiping: true,
        //autoplay: true
    });

    $('.horizontal__slider__inner').slick({
        vertical: false,
        dots: true,
        //autoplay: true
    });
    
    /* Country click handler */
    function countryclick(tld) {

        if ($('#' + tld).length == 1) {
            $('.description').hide();
            $('.worldmap_1').removeClass('active');
            $('#' + tld).show();
            $('#g' + tld).addClass('active');
        }
    }

    jQuery(document).ready(function(){

        jQuery('#map').SVGWorldMap({
            bottom: 20, /* Bottom padding (removes antarctica from map) */
            clickhandler :'countryclick', /* Click handler function name */
            c : {BR:1,RU:1,IN:1,CN:1,US:1,UK:1} /* Country classes */
        });

    });
    
</script>
</body>
</html>
