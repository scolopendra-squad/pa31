<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('aboutUs');
})->name('aboutUs');

Route::get('/resources/{folder}/{file}', function($folder, $file) {
    $file_name = File::get(resource_path() . "/" . $folder . "/" . $file);
    $response = Response::make($file_name);
    if (explode(".", $file)[1] == "css" ){
        $response->header('Content-Type', "text/css");
    } elseif (explode(".", $file)[1] == "js")
        $response->header('Content-Type', "text/js");
    elseif (explode(".", $file)[1] == "scss")
        $response->header('Content-Type', "text/scss");
    elseif (explode(".", $file)[1] == "jpg")
        $response->header('Content-Type', "img/jpg");
    return $response;
});


Route::get('/competence', function () {
    return view('competence');
})->name('competence');

Route::get('/directions', function () {
    return view('directions');
})->name('directions');

Route::get('/lean_six_sigma/directions', function () {
    return view('lean_six_directions');
})->name('lean_six_directions');