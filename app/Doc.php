<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doc extends Model
{
	protected $table = "docs";
    public function quality(){
        return $this->belongsTo('App\Quality', 'quality');
    }
}
